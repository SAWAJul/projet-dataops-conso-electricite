import datetime

from airflow.decorators import dag, task

@task(task_id="print_the_context")
def print_context(ds=None, **kwargs):
    print(ds)
    return "Whatever you return gets printed in the logs"

@dag(start_date=datetime.datetime(2024, 10, 1), schedule="@daily")
def generate_dag():
    run_this = print_context()

generate_dag()