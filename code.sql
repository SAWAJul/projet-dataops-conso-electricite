DROP TABLE IF EXISTS `ensai-2024.dev_julien_consumption.totale_consumption`;

CREATE TABLE `ensai-2024.dev_julien_consumption.totale_consumption` AS (
  WITH conso_totale AS (
    SELECT
      -- dimensions
      code_commune,
      libelle_commune,
      code_postal,
      annee,
      'total' AS secteur,
      -- metrics
      SUM(IF(filiere LIKE 'Electricité', consototale, NULL)) AS Consommation_totale_elec,
      SUM(IF(filiere LIKE 'Gaz', consototale, NULL)) AS Consommation_totale_gaz
    FROM `ensai-2024.dev_christophe_consumption.consumption`
    GROUP BY code_commune, annee, libelle_commune, code_postal
    ORDER BY code_commune, annee
  ),
  production_totale AS (
    SELECT
      code_commune,
      annee,
      SUM(energie_produite_annuelle_autres_filieres_enedis_mwh+
      energie_produite_annuelle_bio_energie_enedis_mwh+
      energie_produite_annuelle_cogeneration_enedis_mwh+
      energie_produite_annuelle_eolien_enedis_mwh+
      energie_produite_annuelle_hydraulique_enedis_mwh+
      energie_produite_annuelle_photovoltaique_enedis_mwh) AS
      Production_totale_elec
    FROM `ensai-2024.dev_christophe_consumption.production`
    GROUP BY code_commune, annee
    ORDER BY code_commune, annee
  )

  SELECT e.code_commune,
      libelle_commune,
      code_postal,
      e.annee,
      secteur,
      Consommation_totale_elec,
      Consommation_totale_gaz,
      Production_totale_elec,
      COALESCE(Production_totale_elec, 0) - COALESCE(Consommation_totale_elec, 0) AS Delta
      FROM 
  conso_totale AS e INNER JOIN production_totale AS g
  ON e.code_commune = g.code_commune AND e.annee = g.annee
)